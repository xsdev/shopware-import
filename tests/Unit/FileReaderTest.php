<?php

namespace Tests\Unit;

use App\Readers\FileReader;
use Tests\TestCase;

class FileReaderTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        $this->reader = new FileReader();
    }

    public function test_it_can_read_json_from_the_file()
    {
        $result = $this->reader->readFromFile('tests/test.json');
        $this->assertCount(2, $result);
    }

    public function test_it_throws_exception_when_no_file_exists()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->reader->readFromFile('tests/nothing');
    }

    public function test_it_can_normalize_raw_input()
    {
        $data = [
            (object)["title" => "First course", "product_line_area" => "demo"],
            (object)["title" => "Second course", "lang" => "de", "product_line_area" => "demo"]
        ];
        $reference = collect([
            ["parent" => "demo", "child" => "First course", "language" => "English", "extra" =>[]],
            ["parent" => "demo", "child" => "Second course",  "language" => "Deutsch", "extra" =>[]]
        ]);

        $result = $this->reader->normalizeCategories($data);
        $this->assertCount(2, $result);
        $this->assertEquals($reference, $result);

    }

    public function test_it_can_read_and_normalize_raw_category_data()
    {
        $result = $this->reader->read(['source' =>'tests/test.json']);

        $reference = collect([
            ["parent" => "demo", "child" => "First course", "language" => "English", "extra" =>[]],
            ["parent" => "demo", "child" => "Second course",  "language" => "Deutsch", "extra" =>[]]
        ]);

        $this->assertCount(2, $result);
        $this->assertEquals($reference, $result);

    }



}
