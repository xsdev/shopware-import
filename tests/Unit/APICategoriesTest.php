<?php

namespace Tests\Unit;

use App\APICategories;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class APICategoriesTest extends TestCase
{


    protected function setUp(): void
    {
        parent::setUp();
        $this->fakeGuzzle = \Mockery::mock(Client::class);
        $this->categories = new APICategories($this->fakeGuzzle);
    }

    /**
     * Yes, I know. Diving too deep into implementation
     */
    public function test_it_can_send_category_creation_request()
    {
        $responseMock = \Mockery::mock(Response::class);
        $responseMock->shouldReceive('getBody')
            ->andReturn(json_encode(
                ['data' => ['name' => 'example']]
            ));

        $this->fakeGuzzle
            ->shouldReceive('request')
            ->withArgs(['POST', 'categories', ['body' => '{"name":"English","parentId":1}']])
            ->once()
        ->andReturn($responseMock);

        $result = $this->categories->create('English');

        $this->assertEquals((object)['name' => 'example'], $result);

    }


    public function test_it_can_truncate_request()
    {
        $categories = \Mockery::mock(
            APICategories::class,
            [ $this->fakeGuzzle ]
        )->makePartial();

        $categories->shouldReceive('getImmediateDescendants')
            ->once()
            ->andReturn([
                (object)['id'=> 2],
                (object)['id'=> 3]]
            );

        $this->fakeGuzzle
            ->shouldReceive('request')
            ->withArgs(['DELETE', 'categories/2'])
            ->once();
        $this->fakeGuzzle
            ->shouldReceive('request')
            ->withArgs(['DELETE', 'categories/3'])
            ->once();

        $categories->truncate();
    }


    public function test_it_can_parse_json_responses()
    {
        $result = $this->categories->dataOnly(json_encode([
            'data' => ['name' => 'example', 'id' => 1]
        ]));

        $this->assertEquals((object)['name' => 'example', 'id' => 1] ,$result);
    }


}
