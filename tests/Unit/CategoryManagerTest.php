<?php

namespace Tests\Unit;

use App\APICategories;
use App\CategoryManager;
use App\Commands\Import;
use GuzzleHttp\Client;
use Tests\TestCase;

class CategoryManagerTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        $this->fakeGuzzle = \Mockery::mock(Client::class);
        $this->manager = new CategoryManager(new APICategories($this->fakeGuzzle));
    }

    public function test_it_can_read_json_from_the_file()
    {
        $data = collect([
            ["parent" => "demo", "child" => "First course", "language" => "English", "extra" =>[]],
            ["parent" => "demo", "child" => "Second course",  "language" => "Deutsch", "extra" =>[]]
        ]);
        $result = $this->manager->filterByLanguage('Deutsch', $data);
        $this->assertCount(1, $result);
        $this->assertEquals("Second course", $result->first()['child']);
    }




}
