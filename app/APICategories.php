<?php

namespace App;

use GuzzleHttp\ClientInterface;

class APICategories
{

    /**
     * APICategories constructor.
     *
     * It's not PSR-18 client. Just guzzle
     * @see \GuzzleHttp\Client;
     * @param ClientInterface $httpClient
     */
    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }


    /**
     * Create category using remote API
     *
     * @param $name
     * @param int $parentId
     * @param array $extra
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($name, $parentId = 1, $extra = []) // I hope that root node id is permanent and never changes
    {
        $res = $this->httpClient->request('POST', 'categories', [
                'body' => json_encode(compact('name', 'parentId') + $extra)
            ]
        );
        return $this->dataOnly($res->getBody());
    }


    /**
     * Truncate categories using remote API
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function truncate()
    {
        $topLevelCategories = $this->getImmediateDescendants(1);

        foreach ($topLevelCategories as $category) {
            $this->httpClient->request('DELETE', 'categories/' . $category->id);
        }
    }


    /**
     * Parse json response and extract data
     *
     * @param $json_string
     * @return mixed
     */
    public function dataOnly($json_string)
    {
        return json_decode($json_string)->data;
    }


    /**
     * Returns all children nodes (non-recursive)
     * for given parent
     *
     * @param $parent
     * @return \Illuminate\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getImmediateDescendants($parent)
    {
        $res = $this->httpClient->request('GET', 'categories');

        $allCategories = collect($this->dataOnly($res->getBody()));
        $children = $allCategories->where('parentId', $parent);
        return $children;
    }
}