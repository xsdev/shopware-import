<?php

namespace App\Providers;

use App\Readers\FileReader;
use App\Readers\ReaderInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ClientInterface::class, function () {
            return new Client([
                'base_uri' => config('api.base_uri'),
                'auth' => [config('api.user'), config('api.password')]
            ]);
        });

        $this->app->singleton(ReaderInterface::class, FileReader::class);

    }
}
