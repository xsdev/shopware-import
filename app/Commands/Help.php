<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;

class Help extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'help';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'It can help you somehow';


    /**
     * Will show help someday
     */
    public function handle()
    {
        $this->error('Sorry, no help at the moment');
    }

}
