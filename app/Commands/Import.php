<?php

namespace App\Commands;

use App\APICategories;
use App\CategoryManager;
use App\Readers\ReaderInterface;
use LaravelZero\Framework\Commands\Command;

class Import extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'import {source}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Import categories from given source to Shopware API host';

    /**
     * Category manipulator
     *
     * @var APICategories
     */
    protected $categories;


    /**
     * Import categories from input source
     * to Shopware API
     *
     * @param ReaderInterface $reader
     * @param CategoryManager $categoryManager
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(ReaderInterface $reader, CategoryManager $categoryManager)
    {

        $categories = $reader->read(['source' => $this->argument('source')]);

        $this->comment('Importing '.$categories->count().' records...');

        $categoryManager->truncateTree();
        
        $this->comment('Existing categories truncated...');

        $languages = $categoryManager->createLanguageCategories();

        $this->comment('Language categories created...');

        $categoryManager->buildTreeForEveryLanguage($languages, $categories);

        $this->question('Import finished!');
    }

}
