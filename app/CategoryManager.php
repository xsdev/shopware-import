<?php

namespace App;


use Illuminate\Support\Collection;

class CategoryManager
{

    public function __construct(APICategories $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Deletes every category except root node
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function truncateTree()
    {
        $this->categories->truncate();
    }

    /**
     * Create language categories under the root node
     *
     * @return Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createLanguageCategories()
    {
        $categories = collect();
        foreach (config('lang.available') as $language) {
            $newCategory = $this->categories->create($language);
            $categories->put($newCategory->id, $language);
        }

        return $categories;
    }

    /**
     * Builds category trees fo every language in passed array/collection
     *
     * @param Collection $languages
     * @param Collection $categories
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function buildTreeForEveryLanguage($languages, $categories)
    {
        foreach ($languages as $categoryId => $language) {

            $localisedCategories = $this->filterByLanguage($language, $categories);
            $this->buildTree($localisedCategories, $categoryId);

        }
    }


    /**
     * Filter categories by given language
     *
     * @param string $language
     * @param Collection $categories
     * @return \Illuminate\Support\Collection
     */
    public function filterByLanguage($language, $categories)
    {
        return $categories->where('language', $language);
    }


    /**
     * Build 2-level hierachical tree for given root node
     *
     * @param Collection $categories
     * @param int $root
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function buildTree($categories, int $root)
    {
        $grouped = $categories->groupBy('parent');

        foreach ($grouped as $name => $children) {
            $parent = $this->categories->create($name, $root);

            foreach ($children as $child) {
                $this->categories->create(
                    $child['child'],
                    $parent->id,
                    $child['extra'] //Additional fields from input source
                );
            }
        }
    }
}