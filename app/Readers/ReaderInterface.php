<?php

namespace App\Readers;


interface ReaderInterface
{
    /**
     * Read categories from given source
     * return format: [
     *  [ parent => string, child => string, language => string, extra => array ],
     * ...]
     *
     *
     * @param array $options
     * @throws \Exception
     *
     * @return \Illuminate\Support\Collection
     */
    public function read($options = []);
}