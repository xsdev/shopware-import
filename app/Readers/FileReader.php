<?php

namespace App\Readers;


use Illuminate\Support\Arr;

class FileReader implements ReaderInterface
{

    /**
     * Reads categories from a file and normalizes output
     *
     * @see ReaderInterface
     * @param array $options
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function read($options = [])
    {

        $importedCategories = $this->readFromFile($options['source']);

        $categories = $this->normalizeCategories($importedCategories);

        return $categories;
    }


    /**
     * Returns array of categories (raw)
     *
     * @param string $file
     * @return array
     */
    public function readFromFile($file)
    {
        if (!is_readable($file)) throw new \InvalidArgumentException('No such file exists');

        $importedJson = file_get_contents($file);
        return json_decode($importedJson);
    }


    /**
     * Transforms raw category format to normalized format
     *
     * @param $importedCategories
     * @return \Illuminate\Support\Collection
     */
    public function normalizeCategories($importedCategories)
    {
        $categories = collect();
        foreach ($importedCategories as $category) {

            $langCode = $category->lang ?? config('lang.default');
            $extraFields = Arr::only(get_object_vars($category), config('reader.extra_fields'));

            $categories->push([
                'parent' => $category->product_line_area,
                'child' => $category->title,
                'language' => config('lang.available')[$langCode],
                'extra' => $extraFields,
            ]);
        }
        return $categories->unique();
    }

}