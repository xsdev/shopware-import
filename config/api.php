<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Base URI
    |--------------------------------------------------------------------------
    |
    | Hostname with api path
    |
    */

    'base_uri'  => env('API_BASE_URI','http://localhost/'),

    /*
    |--------------------------------------------------------------------------
    | API user
    |--------------------------------------------------------------------------
    |
    | Username created in Shopware admin panel
    |
    */

    'user'      => env('API_USER',''),

    /*
    |--------------------------------------------------------------------------
    | API key
    |--------------------------------------------------------------------------
    |
    | API key created in Shopware admin panel
    | Such data should be stored in .env but make no sense for a test task
    |
    */

    'password'  => env('API_KEY','')

];