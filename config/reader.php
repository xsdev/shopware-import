<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Extra fields
    |--------------------------------------------------------------------------
    |
    | Additional fields from input source.
    | Treated as additional fields for second-level category.
    | Simplified a little bit. In real use cases typically field rename required
    |
    */

    'extra_fields' => ['cmsText']
];