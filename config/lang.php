<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Available languages
    |--------------------------------------------------------------------------
    |
    | Two-letter code must match same language code in the input source
    |
    */

    'available' =>[

        'en' => 'English',
        'de' => 'Deutsch'
        
    ],

    /*
    |--------------------------------------------------------------------------
    | Default language
    |--------------------------------------------------------------------------
    |
    | Default language for categories with no language attribute
    |
    */

    'default' => 'en'
];